/// @description Insert description here
// You can write your code in this editor

gamepad_set_axis_deadzone(0, 0.2)

function horizontal() {
	var keyRight	= keyboard_check(vk_right)	|| keyboard_check(ord("D")) || gamepad_button_check(0, gp_padr)
	var keyLeft		= keyboard_check(vk_left)	|| keyboard_check(ord("A")) || gamepad_button_check(0, gp_padl)
	val = keyRight - keyLeft
	val += gamepad_axis_value(0, gp_axislh)
	return val
}

function vertical() {
	var keyDown		= keyboard_check(vk_down)	|| keyboard_check(ord("S")) || gamepad_button_check(0, gp_padd)
	var keyUp		= keyboard_check(vk_up)		|| keyboard_check(ord("W")) || gamepad_button_check(0, gp_padu)
	val = keyDown - keyUp
	val += gamepad_axis_value(0, gp_axislv)
	return val
}

function jump() {
	return gamepad_button_check_pressed(0, gp_face1) || keyboard_check_pressed(vk_space)
}

function interaction() {
	return keyboard_check(vk_z) || gamepad_button_check(0, gp_face3)
}

function interaction_down() {
	return keyboard_check_pressed(vk_z) || gamepad_button_check_pressed(0, gp_face3)
}

function pause() {
	return keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(0, gp_start)
}

function start_game() {
	return gamepad_button_check_pressed(0, gp_face1)
}

function quit_game() {
	return gamepad_button_check_pressed(0, gp_face2)
}

#region GUI
function gui_select() {
	return gamepad_button_check_pressed(0, gp_face1) || keyboard_check_pressed(vk_return)
}
#endregion