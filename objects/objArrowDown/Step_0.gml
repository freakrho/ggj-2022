y += lengthdir_x(1,dir)
dir+=10

if instance_exists(Player){
	if place_meeting(x,y,Player){
		if keyboard_check(vk_down) or keyboard_check(ord("S")){
			fade_trigger=1
		}
	}
}

if fade_trigger = 1{	
	if image_alpha > 0 {
		image_alpha -= 0.01
	}
}

if image_alpha <= 0 {
	instance_destroy();
}