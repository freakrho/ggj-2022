/// @description Insert description here
draw_set_alpha(fade_alpha)
draw_set_font(font)
draw_set_color(color)
h = fa_left
switch h_align {
	case "left":
		h = fa_left
		break
	case "center":
		h = fa_center
		break
	case "right":
		h = fa_right
		break
}
v = fa_left
switch v_align {
	case "top":
		v = fa_top
		break
	case "middle":
		v = fa_middle
		break
	case "bottom":
		v = fa_bottom
		break
}
draw_set_halign(h)
draw_set_valign(v)
draw_text(x, y, text)


if trigger=1{
	if fade_alpha < 1
	fade_alpha+=0.1
}

if fade_alpha>1 fade_alpha=1;

draw_set_alpha(1)