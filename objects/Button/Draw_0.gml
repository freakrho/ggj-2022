/// @description Insert description here
draw_self()
draw_set_font(text_font)
draw_set_color(text_color)
draw_set_halign(fa_center)
draw_set_valign(fa_middle)
draw_text((bbox_right + bbox_left) / 2, (bbox_bottom + bbox_top) / 2, text)
draw_set_halign(fa_left)
draw_set_valign(fa_top)