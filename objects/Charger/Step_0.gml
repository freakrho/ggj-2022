if (place_meeting(x, y, Player)) {
    if (Player.charge == charge) return;

    if (Direction() == Player.floor_dir && Player.IsCrouching()) {
        // charge
        if (!charging) {
            charging = true
            time = 0
            Player.Charging(charge)
        }

        if (time >= charge_time) {
            charging = false
            Player.charge = charge
            Player.EndCharging()
        }
		
		if (!audio_is_playing(sfxCharge)) {
			PlaySFX(sfxCharge)
		}
        time += delta_time / 1000000
    }
}


switch string_lower(dir) {
		case "down":
			key=global.downkey; break;
		case "up":
			key=global.upkey; break;
		case "left":
			key=global.leftkey; break;
		case "right":
			key=global.rightkey; break;
	}