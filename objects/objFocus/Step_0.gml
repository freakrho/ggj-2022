if !instance_exists(Player) exit

if Player.flipped_x = true {
	x = Player.x - dist
} else {
	x = Player.x + dist
}

y = Player.y