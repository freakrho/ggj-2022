/// @description Insert description here
// You can write your code in this editor

if (collided) {
    // victory animation
    if (!audio_is_playing(sfxVictory)) {
        room_goto_next()
    }
} else {
    if (place_meeting(x, y, Player)) {
        collied = true
        audio_stop_sound(global.current_music)
        PlaySFX(sfxVictory)
        show_debug_message(audio_is_playing(sfxVictory))
    }
}