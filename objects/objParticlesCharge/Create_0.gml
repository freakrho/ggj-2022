time = 0
par_interval = 0.05 * 1000000
size_x = 200
size_y = 70

FirstParticleSystem = part_system_create();
part_system_position(FirstParticleSystem, 0, 91.5);

first_particle = part_type_create();
part_type_shape(first_particle, pt_shape_spark);
part_type_scale(first_particle,1,1);
part_type_size(first_particle,.05,.5,-.01,1);
part_type_color1(first_particle, color);
part_type_alpha2(first_particle,0.9,0.5);
part_type_speed(first_particle,0.1,0.5,0,1);
//part_type_gravity(first_particle,0.02,90);
part_type_orientation(first_particle,0,359,10,0,true);
part_type_life(first_particle,100,150);
part_type_blend(first_particle, false);

first_emitter = part_emitter_create(FirstParticleSystem);
part_emitter_region(FirstParticleSystem, first_emitter, x-size_x/2, x+size_x/2, y-size_y/2, y+size_y/2, ps_shape_ellipse, ps_distr_gaussian);
