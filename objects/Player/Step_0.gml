// @description Insert description here
// You can write your code in this editor

_dt = delta_time / 1000000
mv_spd_h = 0
mv_spd_v = 0

PlayerStateMachine()

ElectricForce()

if (!grounded) {
	accely = Game.Gravity() + forcey / mass
	accelx = forcex / mass
} else {
	accelx = 0
	accely = 0
}

speedy += accely * _dt
speedx += accelx * _dt

dx = (speedx + mv_spd_h * Game.pixels_per_unit) * _dt
dy = (speedy + mv_spd_v * Game.pixels_per_unit) * _dt

if (grounded) {
	if (FloorHorizontal()) {
		speedx *= friction_factor
	} else {
		speedy *= friction_factor
	}
}

if (abs(speedx) > max_speed) speedx = sign(speedx) * max_speed;
if (abs(speedy) > max_speed) speedy = sign(speedy) * max_speed;

if (abs(speedx) < 0.1) {
	speedx = 0
}
if (abs(speedy) < 0.1) {
	speedy = 0
}

TiledMovement()

realx += dx
realy += dy

x = round(realx)
y = round(realy)

image_angle = angle


if room=Room1{
	if charge=-1 {
		if (floor_dir == Direction.Up){
			if grounded=1{
				with objTextFade {trigger=1}
			}
		}
	}
}