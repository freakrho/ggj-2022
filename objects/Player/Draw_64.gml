/// @description Insert description here
// You can write your code in this editor

draw_set_halign(fa_left)
draw_set_valign(fa_left)
if (Game.debug) {
	draw_set_color(c_white)
	draw_set_font(FontDEBUG)
	draw_text(0, 0,
	    "State: " + string(state)
	    + "\nCharge: " + string(charge)
	    + "\nGrounded: " + string(grounded)
	    + "\nSpeed: " + string(speedx) + ", " + string(speedy)
	    + "\nForce: " + string(forcex) + ", " + string(forcey)
	    + "\nAccel: " + string(accelx) + ", " + string(accely)
	    + "\nFloor direction: " + FloorDirStr())
}