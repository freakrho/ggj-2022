/// @description Insert description here
// You can write your code in this editor

realx = x
realy = y
speedx = 0
speedy = 0
accelx = 0
accely = 0
flipped_x = false
max_speed = 1500

enum State {
	Idle,
	Walking,
	Falling,
	Crouching,
}

state = State.Idle
charging = false

function SetState(s) {
	state = s

	if (state == State.Idle) {
		sprite_index = sprCat
	} else if (state == State.Walking) {
		sprite_index = sprCatWalking
	} if (state == State.Crouching) {
		sprite_index = sprCatCrouch
	}
}

InitCharacter()

function InputCrouch() {
    h = Input.horizontal()
	v = Input.vertical()
	switch (Player.floor_dir) {
        case Direction.Up:
            return v < 0;
        case Direction.Down:
			return v > 0;
		case Direction.Left:
			return h < 0;
		case Direction.Right:
			return h > 0;
    }

	return false;
}

function IsCrouching() {
	return state == State.Crouching
}

// Floor movement
function HorizontalMovement() {
	var h = 0
	var v = 0
	if (FloorHorizontal()) h = Input.horizontal();
	else v = Input.vertical();

	if (grounded) {
		mv_spd_h = h * move_speed
		mv_spd_v = v * move_speed
	} else {
		mv_spd_h = h * air_move_speed
	}

	if (abs(h) > 0) {
		if (floor_dir == Direction.Up) flipped_x = h > 0;
		else flipped_x = h < 0;
	} else if (abs(v) > 0) {
		if (floor_dir == Direction.Right) flipped_x = v > 0;
		else flipped_x = v < 0;
	}
}

function Charging(c) {
	charging = true
	sprite_index = sprCatCharging
	if (c > 0) {
		instance_create_depth(x, y, 0, objParticlesChargePositive)
	} else {
		instance_create_depth(x, y, 0, objParticlesChargeNegative)
	}
}

function EndCharging() {
	charging = false
	sprite_index = sprCatCrouch
	instance_destroy(objParticlesChargePositive)
	instance_destroy(objParticlesChargeNegative)
}
