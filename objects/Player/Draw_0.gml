/// @description Insert description here
// You can write your code in this editor

if (abs(charge) > 0 && !grounded) {
	arrow_color = c_white
	if (charge > 0) {
		arrow_color = make_color_rgb(69, 140, 69)
	} else if (charge < 0) {
		arrow_color = make_color_rgb(120, 69, 120)
	}
	draw_set_alpha(1)
	draw_set_color(arrow_color)
	force_angle = arctan2(forcey, forcex)
	length = sqrt(power(forcex, 2) + power(forcey, 2)) - 40
	//draw_line_width(x, y, x + length * cos(force_angle), y + length * sin(force_angle), 20)
	//draw_arrow(x, y, x + forcex, y + forcey, 80)
	draw_set_color(c_white)
}

DrawPlayer(sprite_index, image_index, flipped_x, false)

DebugCharacter()
