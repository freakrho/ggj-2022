draw_set_alpha(fade_alpha)
draw_set_color(c_black)
draw_rectangle(0,0,room_width,room_height,0)
draw_set_color(c_white)
draw_set_alpha(1)

if fade_alpha>0 {
fade_alpha-=fade_speed
}

if fade_alpha<=0{
	instance_destroy();
}