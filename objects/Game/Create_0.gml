/// @description Game settings
global.THRESHOLD = 0.1

function Gravity() {
    return gravity * pixels_per_unit;
}

tile_charge = [0, 50, -50]
function GetTileCharge(tile_index) {
    if (tile_index >= 0 && tile_index < array_length(tile_charge)) {
        return tile_charge[tile_index];
    }
    return 0;
}

//global.current_music = -1
//room_goto(Room1)

global.rightkey=Arrow_Right_Key_Light
global.leftkey=Arrow_Left_Key_Light
global.downkey=Arrow_Down_Key_Light
global.upkey=Arrow_Up_Key_Light
wasd=false;