{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 97,
  "bbox_top": 26,
  "bbox_bottom": 73,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 100,
  "height": 100,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d5a9b973-b394-4c5d-8f7b-6fa75db91037","path":"sprites/Space_Key_Light/Space_Key_Light.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d5a9b973-b394-4c5d-8f7b-6fa75db91037","path":"sprites/Space_Key_Light/Space_Key_Light.yy",},"LayerId":{"name":"6693f6e9-dce6-4536-b66a-c17d9e824df1","path":"sprites/Space_Key_Light/Space_Key_Light.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Space_Key_Light","path":"sprites/Space_Key_Light/Space_Key_Light.yy",},"resourceVersion":"1.0","name":"d5a9b973-b394-4c5d-8f7b-6fa75db91037","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Space_Key_Light","path":"sprites/Space_Key_Light/Space_Key_Light.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"794feaf0-030b-4932-a1d3-8c84c84c9f45","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d5a9b973-b394-4c5d-8f7b-6fa75db91037","path":"sprites/Space_Key_Light/Space_Key_Light.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 50,
    "yorigin": 50,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Space_Key_Light","path":"sprites/Space_Key_Light/Space_Key_Light.yy",},
    "resourceVersion": "1.3",
    "name": "Space_Key_Light",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6693f6e9-dce6-4536-b66a-c17d9e824df1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Keys",
    "path": "folders/Sprites/Keys.yy",
  },
  "resourceVersion": "1.0",
  "name": "Space_Key_Light",
  "tags": [],
  "resourceType": "GMSprite",
}