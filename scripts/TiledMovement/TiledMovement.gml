function TiledMovement() {
    function Ground() {
        grounded = true
        if (floor_dir == Direction.Up) {
            angle = 180
        } else if (floor_dir == Direction.Right) {
            angle = 90
        } else if (floor_dir == Direction.Down) {
            angle = 0
        } else if (floor_dir == Direction.Left) {
            angle = 270
        }
    }

    function CheckTileAtPixel(px, py) {
        return tilemap_get_at_pixel(tilemap, px, py) != 0
    }

    function Collides(points) {
        for (i = 0; i < array_length(points); i++) {
            if (points[i]) return true;
        }
        return false;
    }

    function CheckGroundCharges(tiles) {
        for (i = 0; i < array_length(tiles); i++) {
            tile_charge = Game.GetTileCharge(tile_get_index(tiles[i]))
            show_debug_message("Tile charge: " + string(tiles[i]) + "; " + string(charge) + " != " + string(tile_charge))
            if (tile_charge != 0 && sign(charge) != sign(tile_charge)) {
                return false
            }
        }
        return true
    }

    if (abs(accely) > global.THRESHOLD && abs(accelx) > global.THRESHOLD) {        
        if (abs(accely) >= abs(accelx)) {
            if (accely < 0) floor_dir = Direction.Up;
            else floor_dir = Direction.Down;
        } else {
            if (accelx > 0) floor_dir = Direction.Right;
            else floor_dir = Direction.Left;
        }
    }

    if ((FloorVertical() && abs(speedx) > 0) || (FloorHorizontal() && abs(speedy) > 0)) {
        grounded = false
    }

    spd_x = speedx + mv_spd_h
    spd_y = speedy + mv_spd_v

    // Horizontal collisions
    if (dx > 0) { // Right
        right = Right() + 1 + dx
        points = [
            CheckTileAtPixel(right, Top()),
            CheckTileAtPixel(right, CenterY()),
            CheckTileAtPixel(right, Bottom()),
        ]
        if (Collides(points)) {
            if (floor_dir == Direction.Right) {
                Ground()
            }
            speedx = 0
            mv_spd_h = 0
            cell_x = tilemap_get_cell_x_at_pixel(tilemap, right, Bottom())
            dx = tilemap_get_tile_width(tilemap) * cell_x - Right() - 1
        }
    } else if (dx < 0) { // Left
        left = Left() + dx
        points = [
            CheckTileAtPixel(left, Top()),
            CheckTileAtPixel(left, CenterY()),
            CheckTileAtPixel(left, Bottom()),
        ]
        if (Collides(points)) {
            if (floor_dir == Direction.Left) {
                Ground()
            }
            speedx = 0
            mv_spd_h = 0
            cell_x = tilemap_get_cell_x_at_pixel(tilemap, left, Bottom())
            dx = tilemap_get_tile_width(tilemap) * (cell_x + 1) - Left()
        }
    }

    // Vertical collisions
    if (dy > 0) { // Down
        bottom = Bottom() + 1 + dy
        points = [
            CheckTileAtPixel(Left(), bottom),
            CheckTileAtPixel(CenterX(), bottom),
            CheckTileAtPixel(Right(), bottom),
        ]
        if (Collides(points)) {
            if (floor_dir == Direction.Down) {
                Ground()
            }
            speedy = 0
            mv_spd_v = 0
            cell_y = tilemap_get_cell_y_at_pixel(tilemap, Left(), bottom)
            dy = tilemap_get_tile_height(tilemap) * cell_y - Bottom() - 1
        }
    } else if (dy < 0) { // Up
        top = Top() + dy
        points = [
            CheckTileAtPixel(Left(), top),
            CheckTileAtPixel(CenterX(), top),
            CheckTileAtPixel(Right(), top),
        ]
        if (Collides(points)) {
            if (floor_dir == Direction.Up) {
                Ground()
            }
            speedy = 0
            mv_spd_v = 0
            cell_y = tilemap_get_cell_y_at_pixel(tilemap, Left(), top)
            dy = tilemap_get_tile_height(tilemap) * (cell_y + 1) - Top()
        }
    }

    // Check if still on ground
    if (grounded) {
        points = []
        if (floor_dir == Direction.Right) {
            points = [
                CheckTileAtPixel(Right() + 1, Top()),
                CheckTileAtPixel(Right() + 1, CenterY()),
                CheckTileAtPixel(Right() + 1, Bottom()),
            ]
        } else if (floor_dir == Direction.Left) {
            points = [
                CheckTileAtPixel(Left() - 1, Top()),
                CheckTileAtPixel(Left() - 1, CenterY()),
                CheckTileAtPixel(Left() - 1, Bottom()),
            ]
        } else if (floor_dir == Direction.Up) {
            points = [
                CheckTileAtPixel(Left(), Top() - 1),
                CheckTileAtPixel(CenterX(), Top() - 1),
                CheckTileAtPixel(Right(), Top() - 1),
            ]
        } else if (floor_dir == Direction.Down) {
            points = [
                CheckTileAtPixel(Left(), Bottom() + 1),
                CheckTileAtPixel(CenterX(), Bottom() + 1),
                CheckTileAtPixel(Right(), Bottom() + 1),
            ]
        }

        if (!Collides(points)) {
            grounded = false
        }
    }

    // Check if on correct ground
    if (grounded && floor_dir != Direction.Down) {
        tiles = []
        if (floor_dir == Direction.Right) {
            tiles = [
                tilemap_get_at_pixel(tilemap_charges, Right() + 1, Top()),
                tilemap_get_at_pixel(tilemap_charges, Right() + 1, CenterY()),
                tilemap_get_at_pixel(tilemap_charges, Right() + 1, Bottom()),
            ]
        } else if (floor_dir == Direction.Left) {
            tiles = [
                tilemap_get_at_pixel(tilemap_charges, Left() - 1, Top()),
                tilemap_get_at_pixel(tilemap_charges, Left() - 1, CenterY()),
                tilemap_get_at_pixel(tilemap_charges, Left() - 1, Bottom()),
            ]
        } else if (floor_dir == Direction.Up) {
            tiles = [
                tilemap_get_at_pixel(tilemap_charges, Left(), Top() - 1),
                tilemap_get_at_pixel(tilemap_charges, CenterX(), Top() - 1),
                tilemap_get_at_pixel(tilemap_charges, Right(), Top() - 1),
            ]
        } else if (floor_dir == Direction.Down) {
            tiles = [
                tilemap_get_at_pixel(tilemap_charges, Left(), Bottom() + 1),
                tilemap_get_at_pixel(tilemap_charges, CenterX(), Bottom() + 1),
                tilemap_get_at_pixel(tilemap_charges, Right(), Bottom() + 1),
            ]
        }

        if (CheckGroundCharges(tiles)) {
            grounded = false
            show_debug_message("UNGROUNDED")
        }
    }
}
