// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlaySFX(sound_id) {
	audio_play_sound(sound_id, 0, false)
}