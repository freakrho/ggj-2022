// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function InitCharacter() {
	instance_create_depth(x,y,-99,objFade)
	if !instance_exists(objFocus){
	instance_create_depth(x,y,depth,objFocus)
	}
	if !instance_exists(objCamera){
	instance_create_depth(x,y,depth,objCamera)
	}
    tilemap = layer_tilemap_get_id("Collision")
    tilemap_charges = layer_tilemap_get_id("Charges")

	angle = 0
    grounded = false
    bbox_width = bbox_right - bbox_left
    bbox_height = bbox_bottom - bbox_top
	
	function Top() {
	    return y - sprite_yoffset + sprite_get_bbox_top(mask_index)
	}

    function Left() {
        return x + sprite_get_bbox_left(mask_index) - sprite_xoffset
    }

    function Bottom() {
        return y + sprite_get_bbox_bottom(mask_index) - sprite_yoffset
    }

    function Right() {
        return x + sprite_get_bbox_right(mask_index) - sprite_xoffset
    }
    
    function CenterY() {
        return (Top() + Bottom()) / 2
    }

    function CenterX() {
        return (Left() + Right()) / 2
    }

    enum Direction {
        Up,
        Right,
        Down,
        Left,
    }
    floor_dir = Direction.Down

    // Charge
    forcex = 0
    forcey = 0

    function FloorHorizontal() {
        return floor_dir == Direction.Up || floor_dir == Direction.Down
    }

    function FloorVertical() {
        return floor_dir == Direction.Right || floor_dir == Direction.Left
    }

    function FloorDirStr() {
        if (floor_dir == Direction.Up) return "Up"
        if (floor_dir == Direction.Right) return "Right"
        if (floor_dir == Direction.Down) return "Down"
        if (floor_dir == Direction.Left) return "Left"
        return "None"
    }
}
