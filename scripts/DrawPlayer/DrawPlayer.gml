function DrawPlayer(sprite, image, flip_x, flip_y) {
	scale_x = image_xscale
	scale_y = image_yscale
	if (flip_x) scale_x *= -1;
	if (flip_y) scale_y *= -1;
	// Shadow
	// draw_sprite_ext(sprite, image, x + 128, y + 128, scale_x, scale_y, angle, c_black, image_alpha - 0.5)
	// Sprite
	color = c_white
	if (charge > 0) {
		color = make_color_rgb(69, 140, 69)
	} else if (charge < 0) {
		color = make_color_rgb(120, 69, 120)
	}
	
	draw_sprite_ext(sprite, image,  x, y, scale_x, scale_y, image_angle, color, image_alpha)
}
