// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ElectricForce() {
	forcex = 0
	forcey = 0

	if (grounded) return;

	for (i = 0; i < tilemap_get_width(tilemap_charges); i++) {
		for (j = 0; j < tilemap_get_height(tilemap_charges); j++) {
			tile = tilemap_get(tilemap_charges, i, j)
			tile_charge = Game.GetTileCharge(tile_get_index(tile))
			if (abs(tile_charge) > 0) {
				tile_x = (i + 0.5) * tilemap_get_tile_width(tilemap_charges)
				tile_y = (j + 0.5) * tilemap_get_tile_height(tilemap_charges)
				
				dx = (x - tile_x) / Game.pixels_per_unit
				dy = (y - tile_y) / Game.pixels_per_unit
				force = Game.force_constant * charge * tile_charge / (dx * dx + dy * dy)
				angle = arctan2(dy, dx)
				forcex += force * cos(angle)
				forcey += force * sin(angle)
			}
		}
	}
}
