// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerStateMachine(){
	if (state == State.Idle) {
		HorizontalMovement()
		if (abs(mv_spd_h) > 0 || abs(mv_spd_v) > 0) {
			SetState(State.Walking)
			return
		}
		if (Input.jump()) {
			Jump()
			return
		}
		if (!grounded) {
			SetState(State.Falling)
			return
		}
		if (InputCrouch()) {
			SetState(State.Crouching)
			return
		}
	} else if (state == State.Walking) {
		HorizontalMovement()
		if (!audio_is_playing(sfxSteps)) {
			PlaySFX(sfxSteps);
		}
		if (abs(mv_spd_h) == 0 && abs(mv_spd_v) == 0) {
			SetState(State.Idle)
			return
		}
		if (Input.jump()) {
			Jump()
			return
		}
		if (!grounded) {
			SetState(State.Falling)
			return
		}
	} else if (state == State.Falling) {
		HorizontalMovement()
		if (speedy > 0) {
			sprite_index = sprCatFall
		}
		if (grounded) {
			if (abs(mv_spd_h) > 0 || abs(mv_spd_v) > 0) {
				SetState(State.Walking)
			} else {
				SetState(State.Idle)
			}
			return
		}
	} else if (state == State.Crouching) {
		if (!charging && !InputCrouch()) {
			SetState(State.Idle)
			return
		}
	}
}

function Jump() {
	if (floor_dir == Direction.Down) {
		speedy = -jump_speed * Game.pixels_per_unit
	} else if (floor_dir == Direction.Up) {
		speedy = jump_speed * Game.pixels_per_unit
	} else if (floor_dir == Direction.Right) {
		speedx = -jump_speed * Game.pixels_per_unit
	} else if (floor_dir == Direction.Left) {
		speedx = jump_speed * Game.pixels_per_unit
	}
	grounded = false
	SetState(State.Falling)
	PlaySFX(sfxJump)
	sprite_index = sprCatJump
}