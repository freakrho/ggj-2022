// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayBGM(sound_id) {
	if (!audio_is_playing(sound_id)) {
		audio_stop_sound(global.current_music)
		audio_play_sound(sound_id, 0, true)
		global.current_music = sound_id
	}
}