// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function DebugCharacter(){
    if (Game.debug) {
        draw_set_color(c_white)
        draw_arrow(x, y, x + forcex, y + forcey, 5)

        draw_set_color(c_blue)
        draw_set_alpha(0.5)
        draw_rectangle(Left(), Top(), Right(), Bottom(), false)

        draw_set_color(c_red)
        draw_rectangle(Left(), Top(), Left() + 10, Top() + 10, false)
        draw_rectangle(Right() - 10, Top(), Right(), Top() + 10, false)
        draw_rectangle(Left(), Bottom() - 10, Left() + 10, Bottom(), false)
        draw_rectangle(Right() - 10, Bottom() - 10, Right(), Bottom(), false)
        draw_set_alpha(1)
    }
}