{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfxJump.mp3",
  "duration": 0.8091,
  "parent": {
    "name": "SFX",
    "path": "folders/Sounds/SFX.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfxJump",
  "tags": [],
  "resourceType": "GMSound",
}